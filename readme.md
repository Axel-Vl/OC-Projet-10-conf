# Projet 10 

### [Lien vers l'application](http:165.22.196.22)

## Technologies utilisées

* Hébergeur web: DigitalOcean 
* Os serveur: Ubuntu 18.04 
* Base de données: PostgreSQL
![DigitalOcean](img/digitalocean.png)
* Déploiement continu: Travis CI
![TravisCI](img/travisci.png)
* Monitoring: Sentry & New Relic
![Sentry](img/sentry.png)
![NewRelic](img/newrelic.png)


## Configurations utilisées pour nos différentes Technologies:

### Serveur Web - Nginx:

/etc/nginx/sites-available/pur_beurre
```
server {
  
    listen 80; server_name 165.22.196.22;
    root /home/axel/pur_beurre/;

    location /static {
        alias /home/axel/pur_beurre/staticfiles/;
    }

    location / {
        proxy_set_header Host $http_host;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
        proxy_redirect off;
        proxy_pass http://127.0.0.1:8000;
    }

}
```


### Serveur WSGI - Gunicorn et Supervisor:

/etc/supervisor/conf.d/pur_beurre-gunicorn.conf
```
[program:pur_beurre-gunicorn]
command=/home/axel/env/bin/gunicorn projet_8.wsgi:application
user = axel
directory = /home/axel/pur_beurre
autostart = true
autorestart = true
environment = ENV="PRODUCTION", DJANGO_SETTINGS_MODULE="projet_8.settings.production"

```

### Travis CI:

~/pur_beurre/.travis.yml
```yaml
language: python
python:
  - '3.6'

branches:
  only:
    - staging

before_script:
  - pip install -r requirements.txt
  - pip install flake8

env: DJANGO_SETTINGS_MODULE="projet_8.settings.travis"

services:
  - postgresql

script:
  - python manage.py test
  - flake8 --exit-zero

```

### Sentry

~/pur_beurre/settings/production.py
```python
INSTALLED_APPS += [
    'raven.contrib.django.raven_compat',
]

RAVEN_CONFIG = {
    'dsn': 'https://d0b4de3c02744fd1bdb77d21ba996bf6@sentry.io/1475778',
    'release': raven.fetch_git_sha(os.path.dirname(os.pardir)),
}

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'root': {
        'level': 'WARNING',
        'handlers': ['sentry'],
    },
    'formatters': {
        'verbose': {
            'format': '%(levelname)s %(asctime)s %(module)s '
                      '%(process)d %(thread)d %(message)s'
        },
    },
    'handlers': {
        'sentry': {
            'level': 'ERROR',
            'class': 'raven.contrib.django.raven_compat.handlers.SentryHandler',
            'tags': {'custom-tag': 'x'},
        },
        'console': {
            'level': 'DEBUG',
            'class': 'logging.StreamHandler',
            'formatter': 'verbose'
        }
    },
    'loggers': {
        'django.db.backends': {
            'level': 'ERROR',
            'handlers': ['console'],
            'propagate': False,
        },
        'raven': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
        'sentry.errors': {
            'level': 'DEBUG',
            'handlers': ['console'],
            'propagate': False,
        },
    },
}
```

### Cron task:

crontab -e
```
PATH=/usr/local/bin
00 00 * * 7 update_database.sh
```

/usr/local/bin/update_database.sh

```bash
#!/bin/bash 
cd ~
# Remove last week logs and create a new log file for the current week update
rm -rf logs/updatedb.log 
touch logs/updatedb.log

# Activate the virtual env and launch the updatedb django command. 
# All the stdout from Django are redirected to the log file
source env/bin/activate
echo updating database
cd pur_beurre
python manage.py updatedb > ~/logs/updatedb.log

# deactivate the virtual env and exit the cron shell
deactivate
exit
```

updatedb.py

```python
import requests
from string import punctuation, digits
from django.core.management.base import BaseCommand
from django.db.utils import IntegrityError, DataError
from django.core.exceptions import ValidationError
from core.models import Categories, Food


class Command(BaseCommand):
    help = "update the database via OpenFoodFacts API"

    def handle(self, *args, **options):
        categories = Categories.objects.all()

        for category in categories[:10]:
            self.stdout.write("Category: " + category.name)

            food_request_dict = self.api_food_request(category)

            self.save_food_into_db(food_request_dict)

    def api_food_request(self, category) -> dict:
        url = "https://fr.openfoodfacts.org/cgi/search.pl"

        params = {"action": "process",
                  "tagtype_0": "categories",
                  "tag_contains_0": "contains",
                  "tag_0": category.off_id,
                  "page_size": 1000,
                  "json": 1}

        request = requests.get(url, params)

        return request.json()

    def save_food_into_db(self, request_dict):
        food_list = request_dict["products"]

        for food in food_list:
            try:
                food_name = self.string_cleaner(food["product_name"])
            except KeyError:
                pass
            # Brand
            try:
                food_brand = self.string_cleaner(food["brands"].split(',')[0])
            except KeyError:
                pass
            # Nutriscore
            try:
                food_nutriscore = self.string_cleaner(food["nutrition_grades"])
            except KeyError:
                food_nutriscore = "Z"
            # Fat
            try:
                food_fat = float(food["nutriments"]["fat_100g"])
            except (KeyError, TypeError, ValueError):
                food_fat = None
            # Saturated fat
            try:
                food_saturated_fat = float(food["nutriments"][
                                               "saturated-fat_100g"])
            except (KeyError, TypeError, ValueError):
                food_saturated_fat = None
            # Sugars
            try:
                food_sugars = float(food["nutriments"]["sugars_100g"])
            except (KeyError, TypeError, ValueError):
                food_sugars = None
            # Salt
            try:
                food_salt = float(food["nutriments"]["salt_100g"])
            except (KeyError, TypeError, ValueError):
                food_salt = None
            # OpenFoodFacts url
            try:
                food_url = food["url"]
            except KeyError:
                pass
            # OpenFoodFacts image url
            try:
                food_image_url = food["image_url"]
            except KeyError:
                food_image_url = None
            # Categories
            try:
                food_categories = food["categories"].split(',')
            except KeyError:
                pass

            entry = self.update_or_create_food_entry_into_db(food_name,
                                                             food_brand,
                                                             food_nutriscore,
                                                             food_fat,
                                                             food_saturated_fat,
                                                             food_sugars,
                                                             food_salt,
                                                             food_url,
                                                             food_image_url)

            self.update_or_create_relationships_between_food_and_categories(
                    food_categories,
                    entry)

    def update_or_create_food_entry_into_db(self,
                                            name, brand, nutriscore, fat,
                                            saturated_fat, sugars, salt, url,
                                            image_url) -> object:
        try:
            entry = Food.objects.update_or_create(
                    name=name,
                    brand=brand,
                    defaults={
                        "nutriscore": nutriscore,
                        "fat": fat,
                        "saturated_fat": saturated_fat,
                        "sugars": sugars,
                        "salt": salt,
                        "url": url,
                        "image_url": image_url
                    }
            )
            if entry[1] == True:
                self.stdout.write("Created - " + entry[0].name)
            elif entry[1] == False:
                self.stdout.write("Updated - " + entry[0].name)

            return entry[0]

        except(IntegrityError, DataError):
            pass

    def update_or_create_relationships_between_food_and_categories(self,
                                                                   food_categories,
                                                                   entry) -> None:
        try:
            for food_category in food_categories:
                food_category = self.string_cleaner(food_category)
                category = \
                    Categories.objects.get_or_create(name=food_category)[0]
                entry.categories.add(category)
        except (ValidationError, AttributeError):
            pass

    def string_cleaner(self, my_str: str) -> str:
        for character in punctuation:
            my_str = my_str.replace(character, '')
        for character in digits:
            my_str = my_str.replace(character, '')

        my_str = my_str.title().strip()
        return my_str
```

Ici la commande `updatedb` récupere des aliments via l'API OpenFoodFacts puis utilise ces derniers pour,
soit créer de nouvelles entrées dans notre base de données, soit mettre a jouer des entrées déja existantes.
